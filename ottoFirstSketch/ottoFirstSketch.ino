#include <Otto.h>
Otto ottoRobot;

#define LeftLeg 3 // left leg pin
#define RightLeg 11 // right leg pin
#define LeftFoot 10 // left foot pin
#define RightFoot 9 // right foot pin
#define Buzzer 13 //buzzer pin

// 12 11 10 3

void setup() {
  // configurare per camminare
  ottoRobot.init(LeftLeg, RightLeg, LeftFoot, RightFoot, true, Buzzer);
  ottoRobot.home();
}



void loop() {
  ottoRobot.walk(2, 1000, 1);

}
